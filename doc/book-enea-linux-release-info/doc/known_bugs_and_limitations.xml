<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<chapter id="bugs-limitations">
  <title>Known Problems in This Release</title>

  <para>The open source projects are continuously working on correcting
  reported problems. Corrections to bugs detected by Enea are submitted
  upstream, and the corrections are included in Enea Linux regardless of when
  they will be included by the open source project. Remaining issues are
  listed below.<remark>INFO: The <emphasis role="bold">Release-Specific
  Problems</emphasis> section further down is generated from JIRA with
  gen_known_issues.py, but that script is HARDCODED with affectedversion "Enea
  Linux 6" and needs to be adapted when a release info for another EL version
  changes.</remark></para>

  <section id="bugs-limitations-yocto">
    <title>Yocto</title>

    <para>The Yocto Project community uses a Bugzilla database to track
    information on reported bugs and other issues: <ulink
    url="https://bugzilla.yoctoproject.org">
    https://bugzilla.yoctoproject.org</ulink>.</para>
  </section>

  <section condition="hidden" id="bugs-limitations-sdk">
    <title>Toolchain / SDK</title>

    <itemizedlist spacing="compact">
      <listitem>
        <para>Building the kernel might fail if the GNU Make version 3.82 has
        been installed on the host. The mandatory recovery action is to revert
        to version 3.81.<remark>INFO It is possible to hide this entire
        subsection by setting condition hidden on the section
        element</remark></para>
      </listitem>

      <listitem>
        <para>Installing a second toolchain in the same directory as a
        previous one will break the $PATH variable of the first
        one.<remark>LXCR-3624</remark></para>
      </listitem>
    </itemizedlist>
  </section>

  <section condition="hidden" id="bugs-package-manager">
    <title>Smart Package Manager</title>

    <para>Installation of some packages might currently fail when installing
    using the Smart Package Manager. An example is the qemu package. Errors
    can e.g. be reported by smart about conflicts with files from other
    packages.<remark>INFO It is possible to hide this, set condition to hidden
    on the section element</remark></para>

    <para>The recommended solution to this problem is to install the failing
    package using rpm, already available in enea-image-base. In order to
    install it using rpm, first install rpm using the command <command>smart
    install rpm</command>, then download the package to the target, and
    finally on the target use <command>rpm -i
    &lt;packagename&gt;.rpm</command>. If not installed, several dependencies
    can be reported as "Failed dependencies" with lines "x is needed by y". In
    that case, install the required packages "x" found by dependencies by
    using the smart package manager. Add a channel to an accessible directory
    containing all missing rpm packages. Add packages detected to be missing,
    until all dependencies are resolved.<remark>FIXME It would be nice to add
    also a PowerPC example with qemu failing, the example below is for
    ARM.</remark></para>

    <section>
      <title>Example with Failing QEMU Package</title>

      <para>As an example, the qemu installation can show the following
      error.<remark>INFO It is possible to hide this, set condition to hidden
      on the section element</remark></para>

      <note>
        <para>The example below is for ARM, including the RPM names. For
        PowerPC similar errors can occur and other packages are needed.</para>
      </note>

      <para><emphasis role="bold">error: file /var/run from install of
      qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64 conflicts with file from package
      base-files-3.0.14-r89.0.hierofalcon</emphasis></para>

      <para>A solution to this problem is to install the above qemu package
      using rpm, already available in enea-image-base. In order to install it
      using rpm, first install rpm using the command <command>smart install
      rpm</command>, then download the above package from
      &lt;build_dir&gt;/tmp/deploy/rpm/aarch64. On target, use <command>rpm -i
      qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64.rpm</command>. If not installed,
      several needed dependencies can be seen, e.g.:</para>

      <programlisting>error: Failed dependencies:
        libpixman-1-0 &gt;= 0.32.6 is needed by qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64
        libpixman-1.so.0()(64bit) is needed by qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64
        libglib-2.0.so.0()(64bit) is needed by qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64
        libcap2 &gt;= 2.22 is needed by qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64
        libglib-2.0-0 &gt;= 2.40.0 is needed by qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64
        bluez4 &gt;= 4.101 is needed by qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64
        libbluetooth.so.3()(64bit) is needed by qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64
        libcap.so.2()(64bit) is needed by qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64</programlisting>

      <para>Install the required packages found by dependencies by using the
      smart package manager, as specified in the User's Guide, by adding a
      channel to an accessible directory containing all of the above rpm
      packages. Suggestion for installing required packages, as in the
      following example for qemu and ARM:</para>

      <programlisting>smart install bash
        smart install libcap
        smart install bluez4
        smart install libpixman-1-0
        rpm -i qemu-2.1.0+git0+541bbb07eb-r0.0.aarch64.rpm</programlisting>

      <para>Add packages detected to be missing until all dependencies are
      resolved.</para>
    </section>
  </section>

  <section condition="hidden" id="bugs-limitations-target-side-tools">
    <title>Target-Side Tools</title>

    <itemizedlist spacing="compact">
      <listitem>
        <para>The <emphasis role="bold">perf report</emphasis> does not show
        any output for the PandaBoard target.<remark>LXCR-2710</remark></para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">perf top</emphasis> displays for a few
        seconds the error message <literal>Failed to open
        /tmp/perf-388.map</literal>. Any attempt to exit the subsequent window
        causes the system to hang.<remark>LXCR-3113</remark></para>
      </listitem>

      <listitem>
        <para>When running the <command>perf top</command> command on the i.MX
        6Quad SABRE Lite target, the console text may become red after running
        for some time (~ 1 minute).<remark>LXCR-3631</remark></para>
      </listitem>

      <listitem>
        <para>The <emphasis role="bold">powertop --calibrate</emphasis>
        command does not work on the Keystone k2hk-evm
        target.<remark>LXCR-2660</remark></para>
      </listitem>

      <listitem>
        <para>The <emphasis role="bold">powertop</emphasis> command run with
        <emphasis role="bold">--calibrate</emphasis> or -<emphasis
        role="bold">-time</emphasis> arguments may show some warnings on
        PowerPC targets.</para>
      </listitem>

      <listitem>
        <para>If you get an error message like <literal>Cannot load from file
        /var/cache/powertop/saved_results.powertop</literal> when running
        <emphasis role="bold">powertop</emphasis>, there is most likely not
        enough measurement data collected yet. All you have to do is to keep
        powertop running for a certain time.<remark>LXCR-2176, LXCR-2660,
        LXCR-3106</remark></para>
      </listitem>

      <listitem>
        <para>The message <literal>Model-specific registers (MSR) not found
        (try enabling CONFIG_X86_MSR)</literal> appears when you run <emphasis
        role="bold">powertop</emphasis> on <emphasis
        role="bold">non-x86</emphasis> targets. powertop is mainly an x86n
        tool, so it expects X*^_MSR config to be enabled in the kernel. For
        non-x86 targets this config is not available in the kernel, hence,
        powertop warns about it, but the message can be ignored on those
        targets.<remark> LXCR-2176, LXCR-2660, LXCR-3106</remark></para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">powertop</emphasis> issues a message
        <literal>sh: /usr/bin/xset: No such file or directory</literal> when
        it tries to use xset to configure X display preferences but cannot
        find the binary since the image by default contains no X system. The
        message can simply be ignored.<remark>LXCR-2176</remark></para>
      </listitem>
    </itemizedlist>
  </section>

  <section condition="hidden" id="bugs-limitations-virtualization">
    <title>Virtualization</title>

    <itemizedlist spacing="compact">
      <listitem>
        <para><emphasis role="bold">virtualization</emphasis>: CONFIG_BRIDGE
        is not included in the default p2020rdb
        kernel.<remark>LXVTCR-273</remark></para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">lxc</emphasis>:</para>

        <itemizedlist spacing="compact">
          <listitem>
            <para>User namespace is not available on PowerPC targets as it is
            an experimental feature in the 3.8 kernel.</para>
          </listitem>

          <listitem>
            <para>If multiple Linux containers are started from the same
            process and share resources, destroying the containers might
            result in a race condition with error message "Error in
            `./lxc-test-concurrent': double free or corruption (fasttop)"
            followed by an application
            crash."<remark>LXVTCR-365</remark></para>
          </listitem>
        </itemizedlist>
      </listitem>

      <listitem>
        <para><emphasis role="bold">libvirt</emphasis>: Default network does
        not start.<remark>LXVTCR-240</remark></para>
      </listitem>
    </itemizedlist>
  </section>

  <section condition="hidden" id="bugs-limitations-doc">
    <title>Documentation</title>

    <itemizedlist spacing="compact">
      <listitem>
        <para><emphasis role="bold">PDF navigation</emphasis>: When using
        links to open other PDFs, or jump to another place in the same PDF,
        jumping back sometimes fails. This has been observed when opening a
        PDF in Adobe Reader, inside a browser with PDF add-on, as well as when
        the browser is configured to open PDF files in an external PDF reader.
        As a workaround, open the HTML version of the
        document.<remark>LXCR-3283</remark></para>
      </listitem>

      <listitem>
        <para><emphasis role="bold">Internet Explorer (IE) cannot display some
        web pages</emphasis>: It is recommended to use Firefox or another
        non-IE browser for opening external links. If you prefer reading the
        documentation in PDF format in Adobe Reader (not in an Adobe plug-in
        in a browser), remember to configure a non-IE browser as default
        browser to be able to follow all links from within Adobe Reader.
        Example of a link that does not work in IE: <ulink
        url="https://rt.wiki.kernel.org/">https://rt.wiki.kernel.org/</ulink>.
        <remark>LXCR-3281</remark></para>
      </listitem>
    </itemizedlist>
  </section>

  <section condition="hidden" id="bugs-limitations-other">
    <title>Miscellaneous</title>

    <itemizedlist spacing="compact">
      <listitem>
        <para><emphasis role="bold">menuconfig</emphasis> requires <emphasis
        role="bold">Ncurses</emphasis>. If the terminal that pops up
        immediately closes instead of showing the menuconfig interface, check
        that the Ncurses development library is installed.</para>
      </listitem>
    </itemizedlist>
  </section>

  <!-- The file with a section below is autocreated by make init  -->

  <xi:include href="jiraissues_generated.xml"
              xmlns:xi="http://www.w3.org/2001/XInclude" />
</chapter>